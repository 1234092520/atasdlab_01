﻿#include <iostream>

void printBinary(signed char value) {
    for (int i = 7; i >= 0; --i) {
        std::cout << ((value >> i) & 1);
    }
}

int main() {
    signed char a = 5, b = 127;
    signed char result = a + b;
    std::cout << "5 + 127 = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    a = 2; b = -3;
    result = a - b;
    std::cout << "2 - (-3) = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    a = -120; b = -34;
    result = a - b;
    std::cout << "-120 - (-34) = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    result = (unsigned char)(-5);
    std::cout << "(unsigned char)(-5) = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    a = 56; b = 38;
    result = a & b;
    std::cout << "56 & 38 = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    result = a | b;
    std::cout << "56 | 38 = " << (int)result << " (";
    printBinary(result);
    std::cout << ")" << std::endl;

    return 0;
}
