﻿#include <iostream>
#include <bitset>
#include <iomanip>

using namespace std;

#define MANTISSA_SIZE 23
#define EXPONENT_SIZE 8
#define SIGN_SIZE 1
#define BYTE_TO_BITS(x) bitset<8>(x)
#define MANTISSA_TO_BITS(x) bitset<MANTISSA_SIZE>(x)
#define EXPONENT_TO_BITS(x) bitset<EXPONENT_SIZE>(x)

union CustomFloat {
    struct {
        unsigned int mantissa : MANTISSA_SIZE;
        unsigned int exponent : EXPONENT_SIZE;
        unsigned int sign : SIGN_SIZE;
    } parts;
    float value;
    unsigned char bytes[sizeof(float)];
};

int main() {
    float number;
    cout << "Number: ";
    cin >> number;

    CustomFloat cf;
    cf.value = number;

    string getBits, getBytes;
    for (int i = sizeof(CustomFloat) - 1; i >= 0; i--) {
        string temp = BYTE_TO_BITS(cf.bytes[i]).to_string();
        getBits.append(temp);
        getBytes.append(temp + ' ');
    }

    string getSign = cf.parts.sign ? "1 (-)" : "0 (+)";
    string getExponent = EXPONENT_TO_BITS(cf.parts.exponent).to_string();
    string getMantissa = MANTISSA_TO_BITS(cf.parts.mantissa).to_string();

    cout << "Bits: " << getBits << endl;
    cout << "Bytes: " << getBytes << endl;
    cout << "------------------------------------------\n";
    cout << left << setw(5) << "Sign" << setw(12) << " Exponent" << " Mantissa\n";
    cout << "------------------------------------------\n";
    cout << setw(5) << getSign << " " << setw(10) << getExponent << " " << getMantissa << endl;
    cout << "------------------------------------------\n";
    cout << "Size memory: " << sizeof(cf) << " bytes\n";

    return 0;
}
