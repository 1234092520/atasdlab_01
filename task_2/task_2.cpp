﻿#include <iostream>
using namespace std;

int main() {
    signed short number;
    cout << "Number: ";
    cin >> number;

    bool sign = (number >> (sizeof(signed short) * 8 - 1)) & 1;
    signed short magnitude = number & ~(1 << (sizeof(signed short) * 8 - 1));

    cout << "Number: " << number << ", Sign: " << (sign ? "-" : "+") << endl;

    return 0;
}
