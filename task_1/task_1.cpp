﻿#include <iostream>
#include <ctime>
#include <cstring>  

struct DateTimeStruct {
    unsigned short Year;
    unsigned char Month;
    unsigned char Day;
    unsigned char Hour;
    unsigned char Minute;
    unsigned char Second;
};

int main() {
    DateTimeStruct currentTime;

    // Отримання поточного часу 
    std::time_t t = std::time(nullptr);
    std::tm now;
    std::memset(&now, 0, sizeof(std::tm)); // Ініціалізація нулями
    localtime_s(&now, &t);

    //структури DateTimeStruct
    currentTime.Year = now.tm_year + 1900;
    currentTime.Month = now.tm_mon + 1;
    currentTime.Day = now.tm_mday;
    currentTime.Hour = now.tm_hour;
    currentTime.Minute = now.tm_min;
    currentTime.Second = now.tm_sec;

    //поточним часом
    char buffer[100];
    std::strftime(buffer, sizeof(buffer), "%Y-%m-%d %H:%M:%S", &now);
    std::string currentTimeStr(buffer);

    //поточний час
    std::cout << "Current time as string: " << currentTimeStr << std::endl;


    //лінія
    std::cout << std::string(30, '-') << std::endl;
    //size timestruct
    std::cout << "Size of DateTimeStruct structure: " << sizeof(DateTimeStruct) << " bytes" << std::endl;
    //size tm
    std::cout << "Size of tm structure: " << sizeof(std::tm) << " bytes" << std::endl;

    return 0;
}
